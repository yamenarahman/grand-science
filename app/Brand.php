<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $withCount = ['products'];

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function path()
    {
        return "/brands/{$this->id}";
    }
}
