<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Product;
use App\Article;

class MainController extends Controller
{
    public function index()
    {
        $products = Product::with('brand')->latest()->paginate(12);
        $articles = Article::latest()->paginate(12);

        return view('welcome', compact('products', 'articles'));
    }
}
