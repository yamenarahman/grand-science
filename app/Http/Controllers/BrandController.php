<?php

namespace App\Http\Controllers;

use App\Brand;

class BrandController extends Controller
{
    public function show(Brand $brand)
    {
        $brand->load('products');

        return view('brand', compact('brand'));
    }
}
