<?php

namespace App\Http\Controllers;

use App\Subscriber;
use Illuminate\Http\Request;

class SubscriberController extends Controller
{
    public function store(Request $request)
    {
        $attributes = $request->validate(['email' => 'required']);

        Subscriber::create($attributes);

        return back();
    }
}
