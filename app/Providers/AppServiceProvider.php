<?php

namespace App\Providers;

use App\Brand;
use App\Service;
use App\Configuration;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        if (Schema::hasTable('brands')) {
            view()->share('brands', Brand::orderBy('name')->get());
        }

        if (Schema::hasTable('configurations')) {
            view()->share('configurations', Configuration::all());
        }
    }
}
