@extends('layouts.app')

@section('content')
    <div class="banner_inner_content_agile_w3l">

    </div>
    <section class="banner-bottom py-lg-5 py-3">
        <div class="container">
            <div class="inner-sec-shop pt-lg-4 pt-3">
                <div class="row">
                    <div class="col-lg-4 single-right-left ">
                        <div class="grid images_3_of_2">
                            <div class="flexslider1">
                                <ul class="slides">
                                    @foreach(json_decode($product->image) as $image)
                                        <li data-thumb="{{ '/storage/' . $image }}">
                                            <div class="thumb-image">
                                                <img src="{{ '/storage/' . $image }}"
                                                     data-imagezoom="true" class="img-fluid" alt=" ">
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8 single-right-left simpleCart_shelfItem">
                        <h3>{{ $product->name }}</h3>
                        <h4 style="margin-top: 20px;">Brand: {{ $product->brand->name }}</h4>
                        <h4 style="margin-top: 20px;">Model: {{ $product->sku }}</h4>
                        @if($product->price != 0)
                            <p><span class="item_price">EGP {{ $product->price }}</span></p>
                        @endif
                        <div class="rating1">
                            <ul class="stars">
                                <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                        <div class="occasion-cart">
                            <div class="toys single-item singlepage">
                                <form action="#" method="post">
                                    <input type="hidden" name="cmd" value="_cart">
                                    <input type="hidden" name="add" value="1">
                                    <input type="hidden" name="toys_item" value="Farenheit">
                                    <input type="hidden" name="amount" value="575.00">
                                </form>
                            </div>
                        </div>

                    </div>
                    <div class="clearfix"></div>
                    <!--/tabs-->
                    <div class="responsive_tabs">
                        <div id="horizontalTab">
                            <ul class="resp-tabs-list">
                                <li>Description</li>
                            </ul>
                            <div class="resp-tabs-container">
                                <!--/tab_one-->
                                <div class="tab1">
                                    <div class="single_page">
                                        {!! $product->description !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@push('js')
    <script>
        // Can also be used with $(document).ready()
        $(window).load(function () {
            $('.flexslider1').flexslider({
                animation: "slide",
                controlNav: "thumbnails"
            });
        });
    </script>
@endpush
