@extends('layouts.app')

@section('content')
    <div class="banner_inner_content_agile_w3l">
    </div>
    <!-- about -->
    <div class="about">
        <div class="container">
            <h3 class="heading-agileinfo">What We Do<span>We offer extensive medical procedures to outbound and inbound patients.</span>
            </h3>
            <div class="col-md-6 about-w3right">
                <img src="images/g6.jpg" class="img-responsive" alt=""/>
            </div>
            <div class="col-md-6 about-w3left">
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                            <h5 class="panel-title asd">
                                <a class="pa_italic collapsed" role="button" data-toggle="collapse"
                                   data-parent="#accordion" href="#collapseOne" aria-expanded="false"
                                   aria-controls="collapseOne">
                                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span><i
                                            class="glyphicon glyphicon-minus"
                                            aria-hidden="true"></i>{{ optional($configurations->where('attribute', 'what_we_do_tab_title_1')->first())->value  }}
                                </a>
                            </h5>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse" role="tabpanel"
                             aria-labelledby="headingOne" aria-expanded="false"
                             style="height: 0px;">
                            <div class="panel-body panel_text">
                                {{ optional($configurations->where('attribute', 'what_we_do_tab_content_1')->first())->value  }}
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingTwo">
                            <h5 class="panel-title asd">
                                <a class="pa_italic collapsed" role="button" data-toggle="collapse"
                                   data-parent="#accordion" href="#collapseTwo" aria-expanded="false"
                                   aria-controls="collapseTwo">
                                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span><i
                                            class="glyphicon glyphicon-minus"
                                            aria-hidden="true"></i>{{ optional($configurations->where('attribute', 'what_we_do_tab_title_2')->first())->value  }}
                                </a>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel"
                             aria-labelledby="headingTwo" aria-expanded="false"
                             style="height: 0px;">
                            <div class="panel-body panel_text">
                                {{ optional($configurations->where('attribute', 'what_we_do_tab_content_2')->first())->value  }}
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingThree">
                            <h5 class="panel-title asd">
                                <a class="pa_italic collapsed" role="button" data-toggle="collapse"
                                   data-parent="#accordion" href="#collapseThree" aria-expanded="false"
                                   aria-controls="collapseThree">
                                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span><i
                                            class="glyphicon glyphicon-minus"
                                            aria-hidden="true"></i>{{ optional($configurations->where('attribute', 'what_we_do_tab_title_3')->first())->value  }}
                                </a>
                            </h5>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel"
                             aria-labelledby="headingThree" aria-expanded="false"
                             style="height: 0px;">
                            <div class="panel-body panel_text">
                                {{ optional($configurations->where('attribute', 'what_we_do_tab_content_3')->first())->value  }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <!-- //about -->
@endsection
