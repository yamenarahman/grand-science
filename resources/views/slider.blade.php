<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1" class=""></li>
        <li data-target="#myCarousel" data-slide-to="2" class=""></li>
    </ol>
    <div class="carousel-inner" role="listbox">
        <div class="item active"
             style="background: -webkit-linear-gradient(rgba(23, 22, 23, 0.2), rgba(23, 22, 23, 0.5)), url({{ optional($configurations->where('attribute', 'slider_1_image')->first())->value  }}) no-repeat;
                 background: -moz-linear-gradient(rgba(23, 22, 23, 0.2), rgba(23, 22, 23, 0.5)), url({{ optional($configurations->where('attribute', 'slider_1_image')->first())->value  }}) no-repeat;
                 background: -ms-linear-gradient(rgba(23, 22, 23, 0.2), rgba(23, 22, 23, 0.5)), url({{ optional($configurations->where('attribute', 'slider_1_image')->first())->value  }}) no-repeat;
                 background: linear-gradient(rgba(23, 22, 23, 0.2), rgba(23, 22, 23, 0.5)), url({{ optional($configurations->where('attribute', 'slider_1_image')->first())->value  }}) no-repeat;
                 background-size: cover;">
            <div class="container">
                <div class="carousel-caption">
                    <h3>{{ optional($configurations->where('attribute', 'slider_1_title')->first())->value  }}</h3>
                    <p>{{ optional($configurations->where('attribute', 'slider_1_subtitle')->first())->value  }}</p>
                    <h6>{{ optional($configurations->where('attribute', 'slider_1_caption')->first())->value  }}</h6>
                </div>
            </div>
        </div>
        <div class="item item2"
             style="background: -webkit-linear-gradient(rgba(23, 22, 23, 0.2), rgba(23, 22, 23, 0.5)), url({{ optional($configurations->where('attribute', 'slider_2_image')->first())->value  }}) no-repeat;
                 background: -moz-linear-gradient(rgba(23, 22, 23, 0.2), rgba(23, 22, 23, 0.5)), url({{ optional($configurations->where('attribute', 'slider_2_image')->first())->value  }}) no-repeat;
                 background: -ms-linear-gradient(rgba(23, 22, 23, 0.2), rgba(23, 22, 23, 0.5)), url({{ optional($configurations->where('attribute', 'slider_2_image')->first())->value  }}) no-repeat;
                 background: linear-gradient(rgba(23, 22, 23, 0.2), rgba(23, 22, 23, 0.5)), url({{ optional($configurations->where('attribute', 'slider_2_image')->first())->value  }}) no-repeat;
                 background-size: cover;">
            <div class="container">
                <div class="carousel-caption">
                    <h3>{{ optional($configurations->where('attribute', 'slider_2_title')->first())->value  }}</h3>
                    <p>{{ optional($configurations->where('attribute', 'slider_2_subtitle')->first())->value  }}</p>
                    <h6>{{ optional($configurations->where('attribute', 'slider_2_caption')->first())->value  }}</h6>
                </div>
            </div>
        </div>
        <div class="item item3"
             style="background: -webkit-linear-gradient(rgba(23, 22, 23, 0.2), rgba(23, 22, 23, 0.5)), url({{ optional($configurations->where('attribute', 'slider_3_image')->first())->value  }}) no-repeat;
                 background: -moz-linear-gradient(rgba(23, 22, 23, 0.2), rgba(23, 22, 23, 0.5)), url({{ optional($configurations->where('attribute', 'slider_3_image')->first())->value  }}) no-repeat;
                 background: -ms-linear-gradient(rgba(23, 22, 23, 0.2), rgba(23, 22, 23, 0.5)), url({{ optional($configurations->where('attribute', 'slider_3_image')->first())->value  }}) no-repeat;
                 background: linear-gradient(rgba(23, 22, 23, 0.2), rgba(23, 22, 23, 0.5)), url({{ optional($configurations->where('attribute', 'slider_3_image')->first())->value  }}) no-repeat;
                 background-size: cover;">
            <div class="container">
                <div class="carousel-caption">
                    <h3>{{ optional($configurations->where('attribute', 'slider_3_title')->first())->value  }}</h3>
                    <p>{{ optional($configurations->where('attribute', 'slider_3_subtitle')->first())->value  }}</p>
                    <h6>{{ optional($configurations->where('attribute', 'slider_3_caption')->first())->value  }}</h6>
                </div>
            </div>
        </div>
    </div>
    <a
        class="left carousel-control"
        href="#myCarousel"
        role="button"
        data-slide="prev"
    >
        <span class="fa fa-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a
        class="right carousel-control"
        href="#myCarousel"
        role="button"
        data-slide="next"
    >
        <span class="fa fa-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
    <!-- The Modal -->
</div>
