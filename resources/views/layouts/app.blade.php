<!DOCTYPE html>
<html>
<head>
    <title>Grand Science | جراند ساينس</title>
    <!--/tags -->
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta
        name="keywords"
        content="Electronic Balance,Industrial,Moisture,PH-meter & Ion met,Conductivity,Oven,Viscometer,Refract,Ultrasonic,Climatic,Muffle,Sieve,موازين اليكترونية,موازين صناعية, ميزان قياس الرطوبة,اجهزة قياس الاس الهيدروجينى والايونى,اجهزة قياس فاعلية التوصيل,افران تخفيف,جهاز اللزوجة,جهاز قياس معامل الانكسار ,حمام مائى التراسونيك,حضانات للتحكم فى الحرارة والرطوبة,أفران حرق,حضانات طبية,جهاز هزاز مناخل,Shaker,جهاز الطرد المركزى,Centrifuge,جهاز اسكبتروفوتومتر,Spectrophotometer,اجهزة تعقيم بسعات مختلفة,Autoclaves,اجهزة تقطير المياه,distillator,ميكروسكوبات,Microscopes,حمامات الماء,سخانات ومقلبات معملية, Hotplate &magnetic,stirrer,أجهزة تعين نقطة الانصهار ,Melting,اجهزة عد البكتريا ,جهاز فتح الغطاء(العزم) Torque meter,ترمومترات الكترونية,Digital,Thermometer,  جهاز استخلاص الدهون,Soxhelt,ميكروميتر , باكوليز ديجيتال,Equipment coefficient,تجهيزات معامل,كيماويات ومستلزمات المعامل,Glassware,Fume,  كبينة تحضير ميديا,Laminar,  كبينة ميديا أمنة,هاز عينات الهوا, أجهزة أختبارات المعمل,AQUARIA,Behr,Bel Engineering,Consort,Copley Scientific,Elma ultrasonic,Ende Cott's,Euromex,Falc insturment,Fistreem,Funke Gerber,HAFNER,Hanna insturment,IKA,Interscience,IUL insturment,Kruss,Memmert,Mitutyo,Nabertherm,Ohaus,Sampling Systems,TOMY,Viscotech,WTW,Yamato"
    />
    <script type="application/x-javascript">
        addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
    <!--//tags -->
    <link
        href="/css/bootstrap.css"
        rel="stylesheet"
        type="text/css"
        media="all"
    />
    <link href="/css/style.css" rel="stylesheet" type="text/css" media="all"/>
    <link href="/css/font-awesome.css" rel="stylesheet"/>
    <link rel="stylesheet" href="/css/lightbox.css">

    <!-- //for bootstrap working -->
    <link
        href="//fonts.googleapis.com/css?family=Source+Sans+Pro:300,300i,400,400i,600,600i,700"
        rel="stylesheet"
    />
</head>

<body>
    <!-- header -->
    <div class="header" id="home">
        @include('layouts.header')

        <div class="content white">
            @include('layouts.nav')
        </div>
    </div>

@yield('content')


<!-- footer -->
    @include('layouts.footer')

    <div class="footer_wthree_agile">
        <p>
            © 2019 Grand Science. All rights reserved | Design by
            <a href="#">Hala Salah Eldin</a>
        </p>
    </div>
    <!-- //footer -->

    <!-- js -->
    <script type="text/javascript" src="/js/jquery-2.1.4.min.js"></script>
    <script src="/js/jquery.flexslider.js"></script>
    <script>
        $('ul.dropdown-menu li').hover(
            function () {
                $(this)
                    .find('.dropdown-menu')
                    .stop(true, true)
                    .delay(200)
                    .fadeIn(500);
            },
            function () {
                $(this)
                    .find('.dropdown-menu')
                    .stop(true, true)
                    .delay(200)
                    .fadeOut(500);
            }
        );
    </script>
    <script type="text/javascript" src="/js/bootstrap.js"></script>
    <script src="/js/lightbox-plus-jquery.min.js"></script>
    <script src="/js/jquery.flexslider.js"></script>

    @stack('js')
</body>
</html>
