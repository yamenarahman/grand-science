<nav class="navbar navbar-default" role="navigation">
  <div class="container">
    <div class="navbar-header">
      <button
      type="button"
      class="navbar-toggle"
      data-toggle="collapse"
      data-target="#bs-example-navbar-collapse-1"
      >
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href="/">
      <h1><img src="/images/logo.jpg" /></h1>
    </a>
  </div>
  <!--/.navbar-header-->
  <div
  class="collapse navbar-collapse"
  id="bs-example-navbar-collapse-1"
  >
  <nav>
    <ul class="nav navbar-nav">
      <li><a href="/">Home</a></li>
      <li><a href="/about">About</a></li>
      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown"
        >BRANDS <b class="caret"></b
          ></a>
          <ul class="dropdown-menu">
            @foreach ($brands as $brand)
              <li><a href="{{ $brand->path() }}">{{ $brand->name }} ({{ $brand->products_count }})</a></li>
            @endforeach
          </ul>
        </li>

        <li><a href="/services">Services</a></li>
        <li><a href="/gallery">Gallery</a></li>

        <li><a href="/contact">CONTACT US</a></li>
      </ul>
    </nav>
  </div>
  <!--/.navbar-collapse-->
  <!--/.navbar-->
</div>
</nav>