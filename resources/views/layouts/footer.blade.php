<div class="footer_top_agile_w3ls">
    <div class="container">
        <div class="col-md-4 footer_grid">
            <h3>About Us</h3>
            <p>{{ optional($configurations->where('attribute', 'about')->first())->value  }}</p>
        </div>

        <div class="col-md-4 footer_grid">
            <h3>Contact Info</h3>
            <ul class="address">
                <li>
                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                    {{ optional($configurations->where('attribute', 'address')->first())->value  }}
                </li>
                <li>
                    <i class="fa fa-envelope" aria-hidden="true"></i>
                    <a href="mailto:{{ optional($configurations->where('attribute', 'email')->first())->value  }}">
                        {{ optional($configurations->where('attribute', 'email')->first())->value  }}
                    </a>
                </li>
                <li>
                    <i class="fa fa-phone" aria-hidden="true"></i>
                    {{ optional($configurations->where('attribute', 'phone')->first())->value  }}
                </li>
            </ul>
        </div>
        <div class="col-md-4 footer_grid ">
            <h3>Sign up for our Newsletter</h3>
            <p>Get Started For Free</p>
            <div class="footer_grid_right">
                <form action="{{ route('subscribers.store') }}" method="POST">
                    @csrf
                    <input
                    type="email"
                    name="email"
                    placeholder="Email Address..."
                    required=""
                    />
                    <input type="submit" value="Submit" />
                </form>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>