<div class="top_menu_w3layouts">
    <div class="container">
        <div class="header_left">
            <ul>
                <li>
                    <i class="fa fa-map-marker" aria-hidden="true"></i> {{ optional($configurations->where('attribute', 'address')->first())->value  }}
                </li>
                <li>
                    <i class="fa fa-phone" aria-hidden="true"></i> {{ optional($configurations->where('attribute', 'phone')->first())->value  }}
                </li>
                <li>
                    <i class="fa fa-envelope-o" aria-hidden="true"></i>
                    <a href="mailto:{{ optional($configurations->where('attribute', 'email')->first())->value  }}">{{ optional($configurations->where('attribute', 'email')->first())->value  }}</a>
                </li>
            </ul>
        </div>
        <div class="clearfix"></div>
    </div>
</div>