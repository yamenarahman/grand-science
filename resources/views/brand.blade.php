@extends('layouts.app')

@section('content')
    <div class="banner_inner_content_agile_w3l">

    </div>
    <!-- services -->
    <div class="services">
        <div class="container">
            <h3 class="heading-agileinfo">
                <img src="{{ url('storage/' . $brand->image) }}" alt=""
                     class="img img-rounded img-responsive" width="100px" height="100px" style="margin: auto;">
                {{ $brand->name }}
                <span> {{ $brand->description }}</span>
            </h3>

            @foreach ($brand->products->chunk(3) as $collection)
                <div class="services-top-grids">
                    @foreach ($collection as $product)
                        <div class="col-md-4">
                            <div class="grid1">
                                <img src="{{ url('storage/' . json_decode($product->image)[0]) }}"
                                     class="img-responsive"/>
                                <h4><a href="{{ $product->path() }}">{{ $product->name }}</a></h4>
                                <p>{{ $product->caption }}</p>
                            </div>
                        </div>
                    @endforeach
                    <div class="clearfix"></div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
