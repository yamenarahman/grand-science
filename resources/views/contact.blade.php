@extends('layouts.app')

@section('content')
    <div class="banner_inner_content_agile_w3l">

    </div>
    <!-- /inner_content -->
    <div class="banner-bottom">
        <div class="container">
            <div class="inner_sec_info_agileits_w3">
                <h2 class="heading-agileinfo">Mail Us<span>We offer extensive medical procedures to outbound and inbound patients.</span>
                </h2>
                <div class="contact-form">
                    <form action="{{ route('contacts.store') }}" method="POST">
                        @csrf
                        <div class="left_form">
                            <div>
                                <span><label>Name</label></span>
                                <span><input name="name" type="text" class="textbox" required></span>
                            </div>
                            <div>
                                <span><label>E-mail</label></span>
                                <span><input name="email" type="text" class="textbox" required></span>
                            </div>
                            <div>
                                <span><label>Phone</label></span>
                                <span><input name="phone" type="text" class="textbox" required></span>
                            </div>
                        </div>
                        <div class="right_form">
                            <div>
                                <span><label>Message</label></span>
                                <span><textarea name="message" required> </textarea></span>
                            </div>
                            <div>
                                <span><input type="submit" value="Submit" class="myButton"></span>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- /map -->
    <div class="map_w3layouts_agile">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3451.454546330462!2d31.306486815116628!3d30.109804181858593!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x1458159d7c6cc78f%3A0x9fec7d9ccf625949!2zMzYg2YXYpNiz2LPYqSDYp9mE2YbZiNix2IwgQVogWmF5dG91biBBbCBHaGFyYmV5YWgsIEVsLVpheXRvdW4sIENhaXJvIEdvdmVybm9yYXRl!5e0!3m2!1sen!2seg!4v1554040459977!5m2!1sen!2seg"
                width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>

    </div>
    <!-- //map -->
@endsection

