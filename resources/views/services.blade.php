@extends('layouts.app')

@section('content')
<div class="banner_inner_content_agile_w3l">

</div>
<div class="services">
	<div class="container">
		<h3 class="heading-agileinfo">Services<span>We offer extensive medical procedures to outbound and inbound patients.</span></h3>

		@foreach ($services->chunk(3) as $collection)
		<div class="services-top-grids">
			@foreach ($collection as $service)
				<div class="col-md-4">
					<div class="grid2">
						<i class="{{ $service->icon }}" aria-hidden="true"></i>
						<h4>{{ $service->title }}</h4>
						<p>{{ $service->description }}</p>
					</div>
				</div>
			@endforeach
			<div class="clearfix"></div>
		</div>
		@endforeach
	</div>
</div>
@endsection