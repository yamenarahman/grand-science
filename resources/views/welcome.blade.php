@extends('layouts.app')

@section('content')
    @include('slider')
    <!-- about -->
    <div class="agile-about w3ls-section">
        <!-- about-bottom -->
        <div class="agileits-about-btm">
            <h3 class="heading-agileinfo">
                Welcome To Our Company!<span
                >We offer extensive medical procedures for medical centers and
      hospitals.</span
                >
            </h3>
            <div class="container">
                <div class="w3-flex">
                    <div class="col-md-3 col-sm-3 ab1 agileits-about-grid1">
                        <div class="text-center h1">
                            <i class="{{ optional($configurations->where('attribute', 'welcome_block_1_icon')->first())->value }}"
                               aria-hidden="true"></i>
                        </div>
                        <h4 class="agileinfo-head">
                            {{ optional($configurations->where('attribute', 'welcome_block_1_title')->first())->value }}
                        </h4>
                        <p>{{ optional($configurations->where('attribute', 'welcome_block_1_content')->first())->value }}</p>
                    </div>
                    <div class="col-md-3 col-sm-3 ab1 agileits-about-grid2">
                        <div class="text-center h1">
                            <i class="{{ optional($configurations->where('attribute', 'welcome_block_2_icon')->first())->value }}"
                               aria-hidden="true"></i>
                        </div>
                        <h4 class="agileinfo-head">
                            {{ optional($configurations->where('attribute', 'welcome_block_2_title')->first())->value }}
                        </h4>
                        <p>{{ optional($configurations->where('attribute', 'welcome_block_2_content')->first())->value }}</p>
                    </div>
                    <div class="col-md-3 col-sm-3 ab1 agileits-about-grid3">
                        <div class="text-center h1">
                            <i class="{{ optional($configurations->where('attribute', 'welcome_block_3_icon')->first())->value }}"
                               aria-hidden="true"></i>
                        </div>
                        <h4 class="agileinfo-head">
                            {{ optional($configurations->where('attribute', 'welcome_block_3_title')->first())->value }}
                        </h4>
                        <p>{{ optional($configurations->where('attribute', 'welcome_block_3_content')->first())->value }}</p>
                    </div>

                    <div class="col-md-3 col-sm-3 ab1 agileits-about-grid4">
                        <div class="text-center h1">
                            <i class="{{ optional($configurations->where('attribute', 'welcome_block_4_icon')->first())->value }}"
                               aria-hidden="true"></i>
                        </div>
                        <h4 class="agileinfo-head">
                            {{ optional($configurations->where('attribute', 'welcome_block_4_title')->first())->value }}
                        </h4>
                        <p>{{ optional($configurations->where('attribute', 'welcome_block_4_content')->first())->value }}</p>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- //about-bottom -->
    </div>
    <!-- emergency -->
    <div class="emergency_cases_w3ls">
        <div class="emergency_cases_bt">
            <div class="container">
                <div class="emergency_cases_top">
                    <div class="col-md-6 emergency_cases_w3ls_left">
                        <h4>Opening Hours</h4>
                        <h6>
                            Monday - Friday&nbsp;<span class="eme">8.00 - 18.00</span>
                        </h6>
                        <h6>
                            Monday - Saturday&nbsp;<span class="eme">9.00 - 17.00</span>
                        </h6>
                        <h6>
                            Monday - Sunday&nbsp;<span class="eme">9.00 - 15.00</span>
                        </h6>
                    </div>
                    <div class="col-md-6 emergency_cases_w3ls_right">
                        <h4>Emergency Cases</h4>
                        <h5><i class="fa fa-phone"
                               aria-hidden="true"></i>{{ optional($configurations->where('attribute', 'phone')->first())->value  }}
                        </h5>
                        <p>
                            Your treatment plan is designed for steady progress, with every
                            phase promptly implemented.
                        </p>
                    </div>

                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- //emergency -->
    <!-- services -->
    <div class="services">
        <div class="container">
            <h3 class="heading-agileinfo">
                Products<span
                >We offer extensive medical procedures to outbound and inbound
      patients.</span
                >
            </h3>

            @foreach ($products->chunk(3) as $collection)
                <div class="services-top-grids">
                    @foreach ($collection as $product)
                        <div class="col-md-4">
                            <div class="grid1">
                                <img src="{{ url('storage/' . json_decode($product->image)[0]) }}" class="img-responsive"/>
                                <h4><a href="{{ $product->path() }}">{{ $product->name }}</a></h4>
                                <p>{{ $product->caption }}</p>
                            </div>
                        </div>
                    @endforeach
                    <div class="clearfix"></div>
                </div>
            @endforeach
        </div>
    </div>
    <!-- //services -->
    <!-- Clients -->
    <div class="tesimonials">
        <div class="tesimonials_tp">
            <div class="container">
                <div class="tittle_head_w3layouts">
                    <h3 class="heading-agileinfo te">
                        What Clients Say<span class="te"
                        >We offer extensive medical procedures to outbound and inbound
          patients.</span
                        >
                    </h3>
                </div>
                <div class="inner_sec_info_agileits_w3">
                    <div class="test_grid_sec">
                        <div class="col-md-offset-2 col-md-8">
                            <div
                                class="carousel slide two"
                                data-ride="carousel"
                                id="quote-carousel"
                            >
                                <!-- Bottom Carousel Indicators -->
                                <ol class="carousel-indicators two">
                                    <li
                                        data-target="#quote-carousel"
                                        data-slide-to="0"
                                        class="active"
                                    ></li>
                                    <li data-target="#quote-carousel" data-slide-to="1"></li>
                                    <li data-target="#quote-carousel" data-slide-to="2"></li>
                                </ol>

                                <!-- Carousel Slides / Quotes -->
                                <div class="carousel-inner">
                                    <!-- Quote 1 -->
                                    <div class="item active">
                                        <blockquote>
                                            <div class="test_grid">
                                                <div class="col-sm-3 text-center test_img">
                                                    <img
                                                        src="images/ts1.jpg"
                                                        alt=" "
                                                        class="img-responsive"
                                                    />
                                                </div>
                                                <div class="col-sm-9 test_img_info">
                                                    <p>
                                                        Maecenas quis neque libero. Class aptent
                                                        taciti.Lorem ipsum dolor sit amet, consectetur
                                                        adipiscing elit. Etiam auctor nec lacus ut tempor.
                                                        Mauris.
                                                    </p>
                                                    <h6>Sara Lisbon</h6>
                                                </div>
                                            </div>
                                        </blockquote>
                                    </div>
                                    <!-- Quote 2 -->
                                    <div class="item">
                                        <blockquote>
                                            <div class="test_grid">
                                                <div class="col-sm-3 text-center test_img">
                                                    <img
                                                        src="images/ts2.jpg"
                                                        alt=" "
                                                        class="img-responsive"
                                                    />
                                                </div>
                                                <div class="col-sm-9 test_img_info">
                                                    <p>
                                                        Lorem ipsum dolor sit amet. Class aptent
                                                        taciti.Lorem ipsum dolor sit amet, consectetur
                                                        adipiscing elit. Etiam auctor nec lacus ut tempor.
                                                        Mauris.
                                                    </p>
                                                    <h6>Jane Wearne</h6>
                                                </div>
                                            </div>
                                        </blockquote>
                                    </div>
                                    <!-- Quote 3 -->
                                    <div class="item">
                                        <blockquote>
                                            <div class="test_grid">
                                                <div class="col-sm-3 text-center test_img">
                                                    <img
                                                        src="images/ts3.jpg"
                                                        alt=" "
                                                        class="img-responsive"
                                                    />
                                                </div>
                                                <div class="col-sm-9 test_img_info">
                                                    <p>
                                                        Maecenas quis neque libero. Class aptent
                                                        taciti.Lorem ipsum dolor sit amet, consectetur
                                                        adipiscing elit. Etiam auctor nec lacus ut tempor.
                                                        Mauris.
                                                    </p>
                                                    <h6>Alice Williams</h6>
                                                </div>
                                            </div>
                                        </blockquote>
                                    </div>
                                </div>

                                <!-- Carousel Buttons Next/Prev -->
                                <a
                                    data-slide="prev"
                                    href="#quote-carousel"
                                    class="left carousel-control"
                                ><i class="fa fa-chevron-left"></i
                                    ></a>
                                <a
                                    data-slide="next"
                                    href="#quote-carousel"
                                    class="right carousel-control"
                                ><i class="fa fa-chevron-right"></i
                                    ></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- //Clients -->
    <!--/icons-->
    <div class="banner-bottom">
        <div class="container">
            <div class="tittle_head_w3layouts">
                <h3 class="heading-agileinfo">
                    Case Studies<span
                    >We offer extensive medical procedures to outbound and inbound
          patients.</span
                    >
                </h3>
            </div>
            <div class="inner_sec_info_agileits_w3">
                @foreach ($articles->chunk(3) as $collection)
                    @foreach ($collection as $article)
                        <div class="col-md-4 grid_info">
                            <div class="icon_info">
                                <img src="{{ url('storage/' . $article->image) }}" alt=" " class="img-responsive"/>
                                <h5>{{ $article->title }}</h5>
                                <p>{{ str_limit($article->body, 100) }}</p>
                                <a
                                    href="#"
                                    class="read-agileits"
                                    data-toggle="modal"
                                    data-target="#{{ 'articles-' . $article->id }}"
                                >Read More</a
                                >
                            </div>
                        </div>
                    @endforeach
                    <div class="clearfix"></div>
                @endforeach
            </div>
        </div>
    </div>
    <!--//icons-->
    <!-- bootstrap-modal-pop-up -->
    @foreach ($articles as $article)
        <div
            class="modal video-modal fade"
            id="{{ 'articles-' . $article->id }}"
            tabindex="-1"
            role="dialog"
            aria-labelledby="{{ 'articles-' . $article->id }}"
        >
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        {{ $article->title }}
                        <button
                            type="button"
                            class="close"
                            data-dismiss="modal"
                            aria-label="Close"
                        >
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <img src="{{ url('storage/' . $article->image) }}" alt=" " class="img-responsive"/>
                        <p>{{ $article->body }}</p>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
    <!-- //bootstrap-modal-pop-up -->
@endsection
