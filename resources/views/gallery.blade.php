@extends('layouts.app')

@section('content')
<div class="banner_inner_content_agile_w3l">

</div>
<!-- gallery -->
<div class="gallery">
	<h2 class="heading-agileinfo">Our Gallery<span>We offer extensive medical procedures to outbound and inbound patients.</span></h2>
	<div class="container">
		<div class="gallery-grids">
			@foreach ($images as $image)
				<div class="col-md-4 col-sm-4 gallery-grid">
					<div class="grid">
						<figure class="effect-apollo">
							<a class="example-image-link" href="{{ '/storage/' . $image->image }}" data-lightbox="example-set" data-title="{{ $image->description }}">
								<img src="{{ '/storage/' . $image->image }}" alt="" />
								<figcaption>
								<p>{{ $image->caption }}</p>
								</figcaption>
							</a>
						</figure>
					</div>
				</div>
			@endforeach
			<div class="clearfix"> </div>
		</div>
	</div>
</div>
<!-- //gallery -->
@endsection
