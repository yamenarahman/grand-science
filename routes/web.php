<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MainController@index')->name('main');

Route::view('/about', 'about')->name('about');
Route::view('/contact', 'contact')->name('contact');
Route::get('/gallery', 'ImageController@index')->name('gallery');
Route::get('/services', 'ServiceController@index')->name('services');
Route::get('/products/{product}', 'ProductController@show');
Route::get('/brands/{brand}', 'BrandController@show');
Route::post('/contacts', 'ContactController@store')->name('contacts.store');
Route::post('/subscribers', 'SubscriberController@store')->name('subscribers.store');


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
